import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteDownAction, VoteUpAction } from '../models/destinos-viajes-state.model';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input('idx') position: number; //Aqui renombramos la variable 
  @HostBinding('attr.class') cssClass = 'col-md-4';
  //Usamos el eventEmitter de Angular Core esto es opcionalmente un generic pero conviene ponerle el tipo
  //para que este tipado, usamos una variable de tipo salida @output() 
  @Output() clicked: EventEmitter<DestinoViaje>;
  constructor(private store: Store<AppState>) {
    /*Inicialisamos la varible de salida en el constructor, de esta manera le cargamos un objeto a esta propiedad*/
    this.clicked = new EventEmitter();
  }

  ngOnInit() {
  }

  /*Creamos el metodo ir vamos a poner la accion de desencadenamiento del evento, aca vamos a escribir 
    como lo querriamos usar this.clicked.emit() tenemos que tener algun atributo en nuestro objeto que
    nos permite permite disparar evento, queremos emitir al padre que elemento fue clickeado*/
  ir() {
    this.clicked.emit(this.destino)
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false
  }

}
