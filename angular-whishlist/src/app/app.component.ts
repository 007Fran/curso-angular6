import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-whishlist';
  /*Entonces, el observador en verdad va a ser un observador de "strings", 
    porque fíjense que el "next value" que se le pasa, 
    el siguiente valor que se le pasa al observador, 
    es al fin y al cabo una "string", 
    que es una "string" que representa la fecha pero el tipo de dato es un "string", 
    entonces "time" va a ser un observador de un "string", al fin y al cabo. 
    Vamos a ver esto, ¿qué es lo que ocurrió? 
 */
  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()), 1000)
  });

  destinoAgregado(d){

  }
}
