import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  /*Creamos una variable de tipo form-group y la inicializamos
    con Form builder que es una utilidad que nos permite definir 
    o construir el formulario*/
  fg: FormGroup;
  minLongitud = 3;
  SearchResults: string[];

  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter();
    //Aqui definimos que controles vamos a poder utilizar, y nos ayuda a crear un nuevo form-group 
    this.fg = fb.group({
       nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
       ])], 
       url: ['']
    });
    /*Para logearnos a la consola aqui registramos un observable que vamos a usar en nuestra aplicacion
      vamos a observar todo cambio que se haga sobre el formulario*/
    this.fg.valueChanges.subscribe((form: any) => {
          console.log('cambio el formulario: ', form);
          
    });
  }

  ngOnInit() {
    /*Lo que tenemos que hacer es detectar, a medida que me van tipeando en la caja de texto "nombre", 
      vamos a tener que levantar ese texto y usarlo como para buscar sugerencias. Entonces, para eso vamos a hacer 
      "let elemNombre = <HTMLInputElement>",
    */
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    /*lo que vamos a hacer es suscribirnos a cuando nos aprietan una tecla. Para eso tenemos un selector de eventos "fromEvent", de "rxjs",       
      en donde uno pone el elemento y uno pone el evento, aquí; el que nos interesa es "input", cada vez que tocan una tecla.  
    */
    fromEvent(elemNombre, 'input')
    /*
      vamos a usar la función "PIPE", que nos permite hacer un "pipeline", es decir,un flujo de una operación secuencial tras otra, operaciones 
      en serie. Para eso vamos a usar la operación "map", varias operaciones "map", "filter" que las vamos a importar de los operadores de "rxjs",
      para eso vamos a importarlas directamente,"operators", y las que nos van a interesar van a ser "map",
      "filter", "debounceTime", "distinctUntilChanged", "switchMap". Entonces, vamos a usar el primer operador: "map".
    */
   .pipe(
     map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
     filter(text => text.length > 2), 
     /* "fromEventInput, elemNombre". Esto nos genera un observable de eventos,ven que figura observable de eventos, eventos de entrada.
         Son estos eventos, por eso aquí la entrada, cuando haciemos el "pipe" de operaciones a realizar sobre este stream,
         el tipo de dato de entrada de la primer función del "pipe" de eventos,es justamente un "keyboardEvent".
         Es decir, que esto es un stream de eventos de teclado. Cada evento de teclado tiene un "target", que es el "HTMLInputElement",
     */
     debounceTime(200),
     distinctUntilChanged(),
     switchMap(() => ajax('/assets/datos.json'))
   ).subscribe(AjaxResponse => {
     console.log(AjaxResponse.response);     
      this.SearchResults = AjaxResponse.response;
   });

  }

  

  /*nombre:string, url:string --> Vamos a hacer que ahora le llegue por argumento destino-viaje  
    :boolean no va a ser falta que nos retorne nada */
    guardar(nombre:string, url:string):boolean {
      const d = new DestinoViaje(nombre, url);  
      this.onItemAdded.emit(d);         
      return false;
    } 
    
    /*aqui esta validacion recibe un control con el cual se esta ejecutando esta validacion el valor de retorno de los 
      vakidadores siempre es un objeto que tiene una key es un string y un valor que es un booleano esta sintaxis de type script
      para indicar que vamos a devolver un objeto con este formato ejempro 'required': true esto es ese :string es la key del
      diccionario o has que se le llama en java script o similar a json*/
    nombreValidator(control: FormControl): { [s: string]: boolean } {
      let longitud = control.value.toString().trim().length;
      if (longitud > 0 && longitud < 5) {
        return { invalidNombre: true };
      }
      return null;
    }

    /*La funcion la retornamos como validador de retorno  */
    nombreValidatorParametrizable(minLong: number): ValidatorFn {
      return (control: FormControl): { [s: string]: boolean } | null => {
        let longitud = control.value.toString().trim().length;
        if (longitud > 0 && longitud < minLong) {
          return { minLonNombre: true };
        }
        return null;
      }
    }

    

}
