import { v4 as uuid } from 'uuid';

export class DestinoViaje {

	//nombre:string;
	//imagenUrl:string;

	private selected: boolean;
	public servicios: string[];
	id = uuid();

	/*Vamos a usar un atajo para hacer azucar sintetica vamos a declarar en el constructor 
	  las variables publicas y ahorrarnos el seteo de dicha variable a la propiedad subyacente
	   */
	constructor(public nombre: string, public imagenUrl:string, public votes: number = 0){
		this.servicios = ['pileta-picina', 'desayuno'];
	}
	     //this.nombre = n;
		 //this.imagenUrl = u;
	/*Este metodo es para marcarlo seleccionado para encapsular el acceso a dicha variable*/
	setSelected(s: boolean){
        this.selected = s;
	}
	
	/*vamos a crear 2 metodos este es para verificar si esta seleccionado o no*/
	isSelected(): boolean{
        return this.selected;
	}

	voteUp() {
		this.votes++;
	}

	voteDown() {
		this.votes--;
	}
	
}