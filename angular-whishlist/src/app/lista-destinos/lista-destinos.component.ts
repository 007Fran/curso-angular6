import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from '../models/destinos-api.client.model'; 
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../models/destinos-viajes-state.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[]; 
  
  /*nosotros utilizamos un "Store" sobre "AppState", es el estado de la aplicación, pero, 
    nos vamos a suscribir */
  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
     this.onItemAdded = new EventEmitter();
     this.updates = [];
     /*Arrow fuction state estamos diciendo que sólo nos importan 
       las actualizaciones sobre destinos, más puntualmente, 
       de destinos recuerden que era */
     this.store.select(state => state.destinos.favorito)
     .subscribe(d => {
       const fav = d;
       if (d != null) {
        this.updates.push('Se ha elegido a' + d.nombre);
      }
     }); 
  }
  /*destinos: DestinoViaje[];
  constructor() {
   this.destinos = [];*/

   ngOnInit() {    
  }
  /*nombre:string, url:string --> Vamos a hacer que ahora le llegue por argumento destino-viaje  
    :boolean no va a ser falta que nos retorne nada */
  agregado(d: DestinoViaje) {
    //this.destinos.push(new DestinoViaje(nombre, url));     
    this.destinosApiClient.add(d); 
    this.onItemAdded.emit(d);
    this.store.dispatch(new NuevoDestinoAction(d));
    //return false;
  }

  elegido(e: DestinoViaje) {
    /*Aqui marcamos como seleccionado y desmarcamos como seleccionado
    this.destinos.forEach(function (x) {x.setSelected(false); });
    d.setSelected(true);*/
    this.destinosApiClient.elegir(e);
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }




  }

 


